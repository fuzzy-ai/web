/*
 * decaffeinate suggestions:
 * DS102: Remove unnecessary code created because of implicit returns
 * DS207: Consider shorter variations of null checks
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
// HTTPServer.js -- mock interface for testing the fuzzy.ai web interface
//
// Copyright 2014 fuzzy.ai https://fuzzy.ai/
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

const http = require('http')
const https = require('https')

const debug = require('debug')('web:http-server')

const JSON_TYPE = 'application/json; charset=utf8'

class HTTPServer {
  constructor (port, options) {
    if (port == null) { port = 80 }
    this.port = port
    this.options = options
    if (this.options) {
      this.server = https.createServer(this.options, this._handler)
    } else {
      this.server = http.createServer(this._handler)
    }
  }

  start (callback) {
    const onError = function (err) {
      clearListeners()
      callback(err)
    }

    const onListening = function () {
      clearListeners()
      callback(null)
    }

    const clearListeners = () => {
      this.server.removeListener('error', onError)
      return this.server.removeListener('listening', onListening)
    }

    this.server.on('error', onError)

    this.server.on('listening', onListening)

    return this.server.listen(this.port)
  }

  stop (callback) {
    const onError = err => callback(err)

    const onClose = () => callback(null)

    const clearListeners = () => {
      this.server.removeListener('error', onError)
      return this.server.removeListener('close', onClose)
    }

    this.server.on('error', (err) => {
      clearListeners()
      callback(err)
    })

    this.server.on('close', () => {
      clearListeners()
      this.started = false
      callback(null)
    })

    return this.server.close()
  }

  toString () {
    return `[HTTPServer (port=${this.port})]`
  }

  _handler (request, response) {
    let body = ''
    const respond = function (code, body) {
      response.statusCode = code
      if (!response.headersSent) {
        response.setHeader('Content-Type', JSON_TYPE)
      }
      return response.end(JSON.stringify(body))
    }
    request.on('data', chunk => { body += chunk })
    request.on('error', err => respond(500, {status: 'error', message: err.message}))
    return request.on('end', () => {
      request.body = body
      const rel = request.url.slice(1)
      if (rel.match(/error\/\d+/)) {
        const statusCode = parseInt(rel.slice(6), 10)
        return respond(statusCode, {status: http.STATUS_CODES[statusCode]})
      } else if (rel.match(/wait\/\d+/)) {
        const seconds = parseInt(rel.slice(5), 10)
        debug(`Waiting ${seconds} seconds...`)
        const handler = function () {
          debug(`Done waiting ${seconds} seconds; responding`)
          return respond(200, {status: 'OK'})
        }
        return setTimeout(handler, seconds * 1000)
      } else {
        return respond(200, {status: 'OK'})
      }
    })
  }
}

module.exports = HTTPServer
