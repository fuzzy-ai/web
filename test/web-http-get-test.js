/*
 * decaffeinate suggestions:
 * DS102: Remove unnecessary code created because of implicit returns
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
// web-test.js
//
// Test the web module
//
// Copyright 2012, E14N https://e14n.com/
// Copyright 2014-2015, Fuzzy.ai https://fuzzy.ai/
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

const vows = require('vows')
const assert = require('assert')
const debug = require('debug')('web:web-http-get-test')

const web = require('../lib/web')
const webBatch = require('./web-batch')

process.on('uncaughtException', (err) => {
  console.dir(err.stack.split('\n'))
  return process.exit(-1)
})

vows
  .describe('GET over HTTP')
  .addBatch(webBatch({
    'and we make a get request': {
      topic () {
        const { callback } = this
        debug('Starting GET request')
        const url = 'http://localhost:1623/foo'
        debug(`Getting ${url}`)
        web.get(url, (err, res, body) => {
          if (err) {
            debug(`Error getting ${url}: ${err}`)
            callback(err, null, null)
          } else {
            debug(`Success getting ${url}`)
            callback(null, res, body)
          }
        })
        return undefined
      },
      'it works' (err, res, body) {
        assert.ifError(err)
        assert.isObject(res)
        assert.isString(body)
      },
      'and we check the response': {
        topic (res) {
          this.callback(null, res)
          return undefined
        },
        'it has a statusCode' (err, res) {
          assert.ifError(err)
          assert.isNumber(res.statusCode)
          assert.equal(res.statusCode, 200)
        }
      }
    },
    'and we make a get request with a client error': {
      topic () {
        const { callback } = this
        const url = 'http://localhost:1623/error/404'
        web.get(url, (err, res, body) => {
          if (err && (err.statusCode !== 404)) {
            callback(err)
          } else if (!err) {
            callback(new Error('Unexpected success'))
          } else {
            callback(null, err)
          }
        })
        return undefined
      },
      'it works' (err, obj) {
        assert.ifError(err)
      },
      'its error is correct' (err, obj) {
        assert.ifError(err)
        assert.isObject(obj)
        assert.isString(obj.name)
        assert.equal(obj.name, 'ClientError')
        assert.instanceOf(obj, web.ClientError)
        assert.isNumber(obj.statusCode)
        assert.equal(obj.statusCode, 404)
        assert.isObject(obj.headers)
        for (const name in obj.headers) {
          const value = obj.headers[name]
          assert.isString(name)
          assert.isString(value)
        }
        assert.isString(obj.url)
        assert.equal(obj.url, 'http://localhost:1623/error/404')
        assert.isString(obj.verb)
        assert.equal(obj.verb, 'GET')
        assert.isString(obj.body)
        assert.isString(obj.message)
        assert.equal(obj.message, 'GET on http://localhost:1623/error/404 resulted in 404 Not Found')
      }
    },
    'and we make a get request with a server error': {
      topic () {
        const { callback } = this
        const url = 'http://localhost:1623/error/503'
        web.get(url, (err, res, body) => {
          if (err && (err.statusCode !== 503)) {
            callback(err)
          } else if (!err) {
            callback(new Error('Unexpected success'))
          } else {
            callback(null, err)
          }
        })
        return undefined
      },
      'it works' (err, obj) {
        assert.ifError(err)
      },
      'its error is correct' (err, obj) {
        assert.ifError(err)
        assert.isObject(obj)
        assert.isString(obj.name)
        assert.equal(obj.name, 'ServerError')
        assert.instanceOf(obj, web.ServerError)
        assert.isNumber(obj.statusCode)
        assert.equal(obj.statusCode, 503)
        assert.isObject(obj.headers)
        for (const name in obj.headers) {
          const value = obj.headers[name]
          assert.isString(name)
          assert.isString(value)
        }
        assert.isString(obj.url)
        assert.equal(obj.url, 'http://localhost:1623/error/503')
        assert.isString(obj.verb)
        assert.equal(obj.verb, 'GET')
        assert.isString(obj.body)
        assert.isString(obj.message)
        assert.equal(obj.message, 'GET on http://localhost:1623/error/503 resulted in 503 Service Unavailable')
      }
    }
  })).export(module)
