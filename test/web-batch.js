/*
 * decaffeinate suggestions:
 * DS102: Remove unnecessary code created because of implicit returns
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
// web-batch.js
//
// Quickly set up test batches for the web module
//
// Copyright 2016, Fuzzy.ai https://fuzzy.ai/
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

const fs = require('fs')
const path = require('path')

const assert = require('assert')
const _ = require('lodash')
const async = require('async')

const HTTPServer = require('./http-server')

process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0'

const webBatch = function (rest) {
  const top = 'When we set up test servers'
  const batch = {}
  batch[top] = {
    topic () {
      const df = rel => path.join(__dirname, 'data', rel)

      const httpsApp = new HTTPServer(2342, {
        key: fs.readFileSync(df('localhost.key')),
        cert: fs.readFileSync(df('localhost.crt'))
      }
      )

      const httpApp = new HTTPServer(1623)

      async.parallel([
        callback => httpsApp.start(callback),
        callback => httpApp.start(callback)
      ], err => {
        if (err) {
          this.callback(err)
        } else {
          this.callback(null, httpsApp, httpApp)
        }
      })

      return undefined
    },

    'it works' (err, httpsApp, httpApp) {
      assert.ifError(err)
      assert.isObject(httpsApp)
      assert.isObject(httpApp)
    },
    teardown (httpsApp, httpApp) {
      const { callback } = this
      return async.parallel([
        callback => httpsApp.stop(callback),
        callback => httpApp.stop(callback)
      ], err => callback(err))
    }
  }

  _.assign(batch[top], rest)

  return batch
}

module.exports = webBatch
