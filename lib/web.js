// web.js
//
// Wrap http/https requests in a callback interface
//
// Copyright 2012, E14N https://e14n.com/
// Copyright 2014-2015 Fuzzy.ai https://fuzzy.ai/
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

const urlparse = require('url').parse
const http = require('http')
const https = require('https')
const dns = require('dns')
const net = require('net')
const assert = require('assert')
const util = require('util')

const debug = require('debug')('web')
const async = require('async')
const _ = require('lodash')

class ClientError extends Error {
  constructor (url, verb, statusCode, headers, body) {
    super(`${verb} on ${url} resulted in ${statusCode} ${http.STATUS_CODES[statusCode]}`)
    this.url = url
    this.verb = verb
    this.statusCode = statusCode
    this.headers = headers
    this.body = body
    this.name = 'ClientError'
  }
}

class ServerError extends Error {
  constructor (url, verb, statusCode, headers, body) {
    super(`${verb} on ${url} resulted in ${statusCode} ${http.STATUS_CODES[statusCode]}`)
    this.url = url
    this.verb = verb
    this.statusCode = statusCode
    this.headers = headers
    this.body = body
    this.name = 'ServerError'
  }
}

const DEFAULT_TIMEOUT = 0

const nv = {major: 0, minor: 0, patch: 0}

if (process.version != null) { // This would be very unusual!
  const match = process.version.match(/^v(\d+)\.(\d+)\.(\d+)/)
  if (Array.isArray(match)) {
    const numbers = match.slice(1, 4);
    [nv.major, nv.minor, nv.patch] = Array.from(numbers.map(s => parseInt(s, 10)))
  }
}

class WebClient {
  constructor (...args) {
    this.start = this.start.bind(this)
    this.stop = this.stop.bind(this)
    this._getAgent = this._getAgent.bind(this)
    this._setTimer = this._setTimer.bind(this)
    this.request = this.request.bind(this)
    this.get = this.get.bind(this)
    this.post = this.post.bind(this)
    this.head = this.head.bind(this)
    this.put = this.put.bind(this)
    this.patch = this.patch.bind(this)
    this.delete = this.delete.bind(this)
    debug(`WebClient::constructor(${util.inspect(args)})`)

    if (args.length > 0) {
      if (_.isObject(args[0])) {
        const [props] = args
        debug(`props = ${util.inspect(props)}`)
        if (props.timeout != null) {
          this.timeout = props.timeout
        } else {
          this.timeout = DEFAULT_TIMEOUT
        }
      }
    } else if (_.isNumber(args[0])) {
      [this.timeout] = args
    } else {
      this.timeout = DEFAULT_TIMEOUT
    }

    debug(`@timeout = ${this.timeout}`)

    assert((this.timeout === Infinity) || (_.isFinite(this.timeout) && (this.timeout >= 0)),
      'Timeout must be either infinity, a positive number, or zero')

    this._agent = {}
    this._tid = {}
  }

  start (options) {
    const a1 = this._getAgent('http:') // eslint-disable-line no-unused-vars
    const a2 = this._getAgent('https:') // eslint-disable-line no-unused-vars
  }

  stop () {
    const cleanup = protocol => {
      if (this._tid[protocol]) {
        clearTimeout(this._tid[protocol])
        this._tid[protocol] = undefined
      }
      if (this._agent[protocol]) {
        this._agent[protocol].destroy()
        this._agent[protocol] = undefined
      }
    }

    cleanup('http:')
    cleanup('https:')
  }

  _getAgent (protocol) {
    if (!this._agent[protocol]) {
      let agent
      const options =
        {maxSockets: Infinity}

      // Don't do keepalive if we're going to timeout immediately

      if (this.timeout > 0) {
        options.keepAlive = true
      }

      if (protocol === 'http:') {
        agent = new http.Agent(options)
        if (nv.major > 4) {
          agent.createConnection = this._roundRobinConnection
        }
      } else if (protocol === 'https:') {
        agent = new https.Agent(options)
      } else {
        throw new Error(`Unknown protocol ${protocol}`)
      }

      this._agent[protocol] = agent
    }

    // Maybe timeout

    debug(`@timeout = ${this.timeout}`)

    if (_.isFinite(this.timeout) && (this.timeout > 0)) {
      this._setTimer(protocol)
    }

    return this._agent[protocol]
  }

  _setTimer (protocol) {
    assert.ok(_.isFinite(this.timeout), 'Timeout must be finite')
    assert.ok(this.timeout > 0, 'Timeout must be > 0')

    const destroyAgent = () => {
      if (this._agent[protocol] != null) {
        this._agent[protocol].destroy()
        this._agent[protocol] = undefined
      }
    }

    if (this._tid[protocol]) {
      clearTimeout(this._tid[protocol])
      this._tid[protocol] = undefined
    }

    this._tid[protocol] = setTimeout(destroyAgent, this.timeout)
  }

  request (verb, url, headers, reqBody, callback) {
    // Optional body

    let mod
    if (!callback) {
      callback = reqBody
      reqBody = null
    }

    // Optional headers

    if (!callback) {
      callback = headers
      headers = {}
    }

    const parts = urlparse(url)

    if (parts.protocol === 'http:') {
      mod = http
    } else if (parts.protocol === 'https:') {
      mod = https
    } else {
      callback(new Error(`Unsupported protocol: ${parts.protocol}`))
    }

    const options = {
      host: parts.hostname,
      port: parts.port,
      path: parts.path,
      method: verb.toUpperCase(),
      headers
    }

    options.agent = this._getAgent(parts.protocol)

    if (parts.protocol === 'http:') {
      if (nv.major > 4) {
        options.createConnection = this._roundRobinConnection
      }
    }

    // Add Content-Length if necessary

    if (reqBody && (headers['Content-Length'] == null)) {
      headers['Content-Length'] = Buffer.byteLength(reqBody)
    }

    const req = mod.request(options, (res) => {
      if ((res == null)) {
        return callback(new Error('No results'))
      } else {
        let resBody = ''
        res.setEncoding('utf8')
        res.on('data', chunk => { resBody = resBody + chunk })
        res.on('error', err => callback(err, null, null))
        return res.on('end', () => {
          const code = res.statusCode
          if ((code >= 400) && (code < 500)) {
            return callback(new ClientError(url, verb, code, res.headers, resBody))
          } else if ((code >= 500) && (code < 600)) {
            return callback(new ServerError(url, verb, code, res.headers, resBody))
          } else {
            return callback(null, res, resBody)
          }
        })
      }
    })

    if ((req == null)) {
      callback(new Error('No request returned'))
    }

    req.on('error', err => callback(err, null))

    if (reqBody) {
      req.write(reqBody)
    }

    return req.end()
  }

  get (url, headers, callback) {
    return this.request('GET', url, headers, callback)
  }

  post (url, headers, body, callback) {
    return this.request('POST', url, headers, body, callback)
  }

  head (url, headers, callback) {
    return this.request('HEAD', url, headers, callback)
  }

  put (url, headers, body, callback) {
    return this.request('PUT', url, headers, body, callback)
  }

  patch (url, headers, body, callback) {
    return this.request('PATCH', url, headers, body, callback)
  }

  delete (url, headers, callback) {
    return this.request('DELETE', url, headers, callback)
  }

  _roundRobinConnection (options, callback) {
    debug('In _roundRobinConnection()')

    return async.waterfall([
      callback =>
        // Get all addresses
        dns.lookup(options.host, {all: true}, callback),
      function (addresses, callback) {
        debug(require('util').inspect(addresses))

        let connection = null
        let lastError = null

        const canConnect = function (address, callback) {
          let socket = null

          const coptions = {
            host: address.address,
            port: options.port,
            family: address.family
          }

          debug(coptions)
          debug(`Checking connection for ${address}`)

          const onConnect = function () {
            debug(`Got connection for ${address}`)
            clearListeners()
            connection = socket
            debug(connection)
            callback(true) // eslint-disable-line standard/no-callback-literal
          }

          const onError = function (err) {
            clearListeners()
            debug(`Error getting connection for ${address}`)
            lastError = err
            debug(err)
            callback(false) // eslint-disable-line standard/no-callback-literal
          }

          const clearListeners = function () {
            debug('Clearing event listeners')
            socket.removeListener('connect', onConnect)
            return socket.removeListener('error', onError)
          }

          debug(`Creating socket for ${address}`)

          socket = net.createConnection(coptions)

          debug(`Done creating socket for ${address}`)

          if (socket != null) {
            debug('Have a socket')
            debug(socket)
            socket.on('connect', onConnect)
            return socket.on('error', onError)
          } else {
            debug('Got no socket back')
            return callback(false) // eslint-disable-line standard/no-callback-literal
          }
        }

        debug('Checking addresses for connection')
        debug(addresses)

        return async.detectSeries(_.shuffle(addresses), canConnect, (addr) => {
          debug('Finished checking')
          if (addr != null) {
            debug(`Got a result for ${addr}; returning connection ${connection}`)
            return callback(null, connection)
          } else {
            debug(`Didn't get a connection; returning error ${lastError}`)
            return callback(lastError)
          }
        })
      }
    ], callback)
  }
}

const defaultClient = new WebClient()

module.exports = {
  web (...args) { return defaultClient.request(...Array.from(args || [])) },
  get (...args) { return defaultClient.get(...Array.from(args || [])) },
  post (...args) { return defaultClient.post(...Array.from(args || [])) },
  head (...args) { return defaultClient.head(...Array.from(args || [])) },
  put (...args) { return defaultClient.put(...Array.from(args || [])) },
  del (...args) { return defaultClient.delete(...Array.from(args || [])) },
  start (...args) { return defaultClient.start(...Array.from(args || [])) },
  stop (...args) { return defaultClient.stop(...Array.from(args || [])) },
  ClientError,
  ServerError,
  WebClient
}
